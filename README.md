# Banners

NPM/Gulp based banner build setup that allows for easy build of similar HTML banners by variation and size

# Banner Guidelines

## Variations

The variations of a similar set of banners. For example, the concept is essentially the same but may have different content. Each variation is named uniquely.

## Sizes

Sizes can be named whatever you like but it's easiest to just use the dimensions. Here's some common IAB sizes below.

## 3rd party Platform compliance:

When naming the banner, the 3rd party platform specific code can be injected straight from the outset. This means the same banner can be repurposed for multiple platforms. At the time of writing the banner is compliant with below platforms. Simply add these names as suffix (*with underscore*) to the banner dimensions, e.g. for DCS, name the folder 300x250**_DCS** and for SIZMEK, 300x250**_SIZMEK**. For generic banners or DCM, just name the folder as its dimensions - 300x250.  

- DCS (DoubleClick Studio)
- DCM (DoubleClick Manager)
- ADFORM (Google Adform)
- SIZMEK (Sizmek (previously MediaMind))

# Quickstart

## Install Node.JS and its packages:

> *OSX Users Please Read*  
> You'll need to setup your Node install correctly so you do not use sudo. Please see [this guide](http://michael-kuehnel.de/node.js/2015/09/08/using-vm-to-switch-node-versions.html) before proceeding or you will not be able to run without `sudo` commands. It uses NVM to install and manage Node.

- Install node using the above method
- Run `npm install gulp -g` to install **gulp.js** (the build system)
- Run `npm install` to install all the project dependencies (node packages)

## Command line tasks

### Important CLI tasks first:

- `gulp serve`: Launches a browser-sync process that monitors file changes that get auto-reloaded in the browser. Useful for rapid creative changes. This command is to be run for local production of banners.
- `gulp build` - will create banners for production, ready to be distributed.
- `gulp zip`: creates zipped version of build files, ready to be uploaded to 3rd party platforms. 

### All CLI tasks:

- `gulp`: will run the default task which will tell you what the current package version is
- `gulp clean`: clears the **/dist** and **/dev** folders wiping out all build and distribution
- `gulp lint`: runs eslint JavaScript linter
- `gulp build`: Will create a build of each variant size and place them in the **/dev** folder performing the following:
  + `gulp clean`
  + `gulp lint`
  + concat all CSS and reference result file in HTML file
  + concat all JS and reference result file in HTML file
  + copy all assets except **assets/sprites** folder
  + copy HTML file
- `gulp serve`: Launches a browser-sync process that monitors file changes that get auto-reloaded in the browser. Useful for rapid creative changes.
  + `gulp build`
  + runs browser-sync
  + watches src files
  + does _not_ watch files in `/sprite` folders
- `gulp zip`: Will create the distribution files ready for publishing. Does the following:
  + `gulp build`
  + zips up each variant size
  + places zips in **/dist** folder

# Build Architecture

- Node.js
- Gulp.js (automation)
- [Greensock Animation Library](http://greensock.com/gsap)

## Directory Structure

The following are the folders in the repository. Note that the structure and folder names are very important here and if modified will require modifications to the gulp tasks.

- **readme.md**: this file
- **package.json**: node build configuration. You can increment the version in this file to increment the result zip version
- **gulpfile.js**: Gulp.js command file to run automation tasks
- **.editorconfig**: coding style guidance
- **.gitattributes**: git attributes file
- **.gitignore**: list of things that will not be placed into the repository
- **src**: all source files
  - **global**: global source files used in all banners
  - **variants**: banner variants
    + **sizes**: each variant banner size
      * **.html**: banner HTML file
      * **screen.css**: banner CSS
      * **ad_base.js**: JavaScript file
      * **assets**: all image assets to be distributed
        - **sprites**: images to be consolidated into a spritesheet using `gulp makesprites`
- **backups**: JPG backups for all variants and sizes

The following are folders created when the environment is setup and/or gulp commands run:

- **dev**: compiled banners for debugging and preview. Created with `gulp build`
- **dist**: zipped banners for distribution. Created with `gulp zip`
- **node_modules**: Node.js modules from `npm install`
