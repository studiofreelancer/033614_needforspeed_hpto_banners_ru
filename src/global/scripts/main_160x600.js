// // variables:
var text_a,
    text_a_DOM,
    text_b,
    text_b_DOM,
    copyObject = [{
        a: "THE STAKES",
        b: "ARE HIGH"
    }, {
        a: "THE HEAT",
        b: "IS ON"
    }, {
        a: "WIN AGAINST",
        b: "ALL ODDS"
    }],
    ctaContainer,
    cta,
    ctaDOM,
    cta_copy = "BUY NOW",
    cta_base,
    cta_base_over,
    cta_base_masker,
    frameCounter = 0,
    main_bg,
    ageRate,
    carContainer,
    redCar,
    smoke_left,
    smoke_right,
    Grid_texture,
    NFSP_Logo,
    logoDOM,
    rollverStateReady = false;
var cta_gapper = 15;
var cta_sideSpace = 13;



//manifest - mandatory object   
function setupManifest() {
    manifest = [
        {
            src: "./" + pathCheck + "bg.jpg",
            id: 'bg.jpg',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "ageRate.png",
            id: 'ageRate.png',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "Grid_texture.png",
            id: 'Grid_texture.png',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "NFSP_Logo.png",
            id: 'NFSP_Logo.png',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "redCar.png",
            id: 'redCar.png',
            type: createjs.LoadQueue.IMAGE
        },
        {
            src: "./" + pathCheck + "smoke.png",
            id: 'smoke.png',
            type: createjs.LoadQueue.IMAGE
        }
    ];


    // ***************** moving text:

    text1_a = document.createElement('div');
    text1_a.classList.add('main_copy');
    document.getElementById('container').appendChild(text1_a);
    text1_a_DOM = new createjs.DOMElement(text1_a);
    text1_a_DOM.y = 40;
    // // ***************** bottom text:

    text1_b = document.createElement('div');
    text1_b.classList.add('main_copy');
    document.getElementById('container').appendChild(text1_b);
    text1_b_DOM = new createjs.DOMElement(text1_b);
    // set initial copy:
    text1_a.innerHTML = "j";
    text1_b.innerHTML = "j";
    text1_a.style.left = -(canvas.offsetWidth) + "px";
    text1_b.style.left = canvas.offsetWidth + "px";
    // *****************************************

    cta = document.createElement('div');
    cta.innerHTML = cta_copy;
    cta.classList.add('cta');
    document.getElementById('container').appendChild(cta);

}
//numOfLoops = 3; // - can be overwritten here | default is 3

// already added to stage are Containers:middleContainer, bottomContainer, topContainer
// USAGE: middleContainer.addChild(childName)

//*******************************************
// ***** add custom assets:
//*******************************************
function addCustomAssets() {


    // all visual graphics:
    main_bg = new createjs.Bitmap(loadedImages['bg.jpg']);
    main_bg.regX = main_bg.x = canvas.offsetWidth / 2;
    main_bg.regY = main_bg.image.width / 2;
    main_bg.y = main_bg.image.width / 2 - 170;

    ageRate = new createjs.Bitmap(loadedImages['ageRate.png']);
    // ageRate.alpha = 0;

    carContainer = new createjs.Container();
    redCar = new createjs.Bitmap(loadedImages['redCar.png']);

    smoke_left = getSmoke();
    smoke_left.x = 20;
    smoke_left.y = 120;

    smoke_right = getSmoke();
    smoke_right.x = 80;
    smoke_right.y = 140;
    smoke_right.scaleX = smoke_right.scaleY = smoke_left.scaleX = smoke_left.scaleY = 1.5;

    carContainer.addChild(smoke_left, smoke_right, redCar);
    carContainer.regX = carContainer.x = canvas.offsetWidth / 2;
    carContainer.regY = canvas.offsetHeight / 2;
    carContainer.y = canvas.offsetHeight / 2;
    // carContainer.x = carContainer.x+25;
    // carContainer.scaleX = carContainer.scaleY = .9;

    Grid_texture = new createjs.Bitmap(loadedImages['Grid_texture.png']);

    // *************** logo: EA or Origin
    NFSP_Logo = loadedImages['NFSP_Logo.png'];
    document.getElementById('container').appendChild(NFSP_Logo);

    // ********************** CTA:
    ctaContainer = new createjs.Container();
    ctaDOM = new createjs.DOMElement(cta);
    ctaContainer.alpha = 0;
    cta_base = new createjs.Shape();
    cta_base_over = new createjs.Shape()
    cta_base_masker = new createjs.Shape();
    ctaStroke = new createjs.Shape();
    ctaContainer.addChild(cta_base, cta_base_over, ctaDOM);

    // white_base = new createjs.Shape();
    // ******************************

    bottomContainer.addChild(main_bg);
    middleContainer.addChild(carContainer, Grid_texture, ctaContainer, ageRate);
    topContainer.addChild(text1_a_DOM, text1_b_DOM);
    // console.log(text_final_frame.offsetWidth)

    copyIsSet();
    TweenLite.delayedCall(.4, frame_1_anim);
    console.log("canvas.offsetWidth " + canvas.offsetWidth)
}

var copyIsSet = function() {}

//*******************************************
// ***** add custom animations:
//*******************************************

// **** FRAME 1:
var frame_1_anim = function() {
    // **** set test original states *******************
    // text_final_frame.style.;

    // ******* cta base position
    ctaContainer.x = canvas.offsetWidth + cta_gapper + cta_sideSpace;
    ctaContainer.y = 150;
    cta_base.graphics = getCTA_base('#ff0033');
    cta_base_over.graphics = getCTA_base('#fff');
    cta_base_masker.graphics = getCTA_base('#1d0048');
    cta_base_masker.x = -(cta.offsetWidth + (cta_sideSpace * 2) + (cta_gapper * 2));
    cta_base_over.mask = cta_base_masker;
    // **************************************************

    TweenLite.to(main_bg, 7, {
        scaleX: 1.05,
        scaleY: 1.05,
        ease: Cubic.easeOut
    });
    TweenLite.to(carContainer, 7, {
        scaleX: 1.2,
        scaleY: 1.2,
        y: carContainer.y + 30,
        ease: Cubic.easeOut
    });
    animateSmoke_left();
    animateSmoke_right();
    textAnim();
}

// **** FRAME 2:
var frame_2_anim = function() {
    TweenLite.to(main_bg, 1, {
        y: main_bg.y + 70,
        ease: Cubic.easeInOut
    });
    TweenLite.to(carContainer, 1, {
        y: carContainer.y + 70,
        ease: Cubic.easeInOut
    });

    TweenLite.to(NFSP_Logo, 1, {
        css: {
            top: "20px"
        },
        ease: Cubic.easeInOut
    });

    console.log((canvas.offsetWidth - (cta.offsetWidth)) / 2)
    TweenLite.to(ctaContainer, .5, {
        x: (canvas.offsetWidth - (cta.offsetWidth)) / 2,
        delay: 1,
        alpha: 1,
        ease: Cubic.easeOut,
        onComplete: setupRollStates
    });
}


function textAnim() {
    text1_a.style.left = -(canvas.offsetWidth) + "px";
    text1_b.style.left = canvas.offsetWidth + "px";
    text1_a.innerHTML = copyObject[frameCounter].a;
    text1_b.innerHTML = copyObject[frameCounter].b;
    text1_b_DOM.y = text1_a_DOM.y + text1_a.offsetHeight;
    moveText(canvas.offsetWidth, function() {
        frameCounter++;
        if (frameCounter < copyObject.length) {
            textAnim();
        } else {
            frame_2_anim();
        }
    }, 13);
}

function setupRollStates() {
    rollverStateReady = true;
    document.getElementById('exitButton').addEventListener('mouseover', onMouseOver);
    document.getElementById('exitButton').addEventListener('mouseout', onMouseOut);

}

var onMouseOver = function() {
    TweenLite.to(cta_base_masker, .4, {
        x: 0,
        ease: Cubic.easeOut
    });
    TweenLite.to(cta, .4, {
        css: {
            color: '#fa1c3b'
        },
        ease: Cubic.easeOut
    });

    if (rollverStateReady) {
        rollverStateReady = false;
        smoke_left.smoke_1.alpha = 1;
        smoke_left.smoke_2.alpha = 1;
        smoke_left.smoke_3.alpha = 1;
        animateSmoke_left();
    }
}

var onMouseOut = function() {
    TweenLite.to(cta_base_masker, .4, {
        x: -(cta.offsetWidth + (cta_sideSpace * 2) + (cta_gapper * 2)),
        ease: Cubic.easeIn
    });
    TweenLite.to(cta, .4, {
        css: {
            color: '#fff'
        },
        ease: Cubic.easeIn
    });
}

// custom:
// *********************************


//*******************************************
// ***** check for loops:
//*******************************************
function checkForLoops() {
    loopCount++;
    if (loopCount < numOfLoops) {
        TweenLite.delayedCall(2, loopFrame);
    }
}

function loopFrame() {

}


