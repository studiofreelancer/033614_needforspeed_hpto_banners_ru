function zoomIn(id, callback, fadeOut) {
    TweenLite.to(id, .25, {
        scaleX: 1,
        scaleY: 1,
        alpha: 1,
        // ease: Cubic.easeIn,
        onComplete: function() {
            TweenLite.to(id, 2, {
                scaleX: .9,
                scaleY: .9
            // ,ease: Cubic.easeOut,
            });
            fadeOut = typeof fadeOut !== 'undefined' ? fadeOut : true;
            TweenLite.to(id, .5, {
                delay: 1.5,
                alpha: fadeOut === true ? 0 : 1,
                overwrite: 0,
                ease: Cubic.easeOut,
                onComplete: function() {

                    if (typeof callback == "function") {
                        callback();
                    }
                }
            });
        }
    });
}





function getCTA_base(clr) {
    // console.log(cta.offsetHeight)
    var cta_base = new createjs.Graphics();
    cta_base.setStrokeStyle(1);
    cta_base.beginStroke('#fa1c3b');
    cta_base.beginFill(clr);
    cta_base.moveTo(ctaDOM.x - cta_sideSpace, ctaDOM.y - 2)
        .lineTo(ctaDOM.x + cta.offsetWidth + cta_sideSpace + cta_gapper, ctaDOM.y - 2)
        .lineTo(ctaDOM.x + cta.offsetWidth + cta_sideSpace, ctaDOM.y + cta.offsetHeight + 2)
        .lineTo(ctaDOM.x - cta_sideSpace - cta_gapper, ctaDOM.y + cta.offsetHeight + 2)
        .lineTo(ctaDOM.x - cta_sideSpace, ctaDOM.y - 2);
    return cta_base;
}

function getImageBounds(img, onlyGetBounds) {
    onlyGetBounds = typeof onlyGetBounds !== 'undefined' ? onlyGetBounds : false;
    var w = img.image.width;
    var h = img.image.height;

    ctx.drawImage(img.image, 0, 0, w, h);
    // ctx.createImageData(w, h)

    var idata = ctx.getImageData(0, 0, w, h),
        buffer = idata.data,
        buffer32 = new Uint32Array(buffer.buffer),
        x,
        y,
        x1 = w,
        y1 = h,
        x2 = 0,
        y2 = 0;

    // get left edge
    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            if (buffer32[x + y * w] > 0) {
                if (x < x1)
                    x1 = x;
            }
        }
    }

    // get right edge
    for (y = 0; y < h; y++) {
        for (x = w; x >= 0; x--) {
            if (buffer32[x + y * w] > 0) {
                if (x > x2)
                    x2 = x;
            }
        }
    }

    // get top edge
    for (x = 0; x < w; x++) {
        for (y = 0; y < h; y++) {
            if (buffer32[x + y * w] > 0) {
                if (y < y1)
                    y1 = y;
            }
        }
    }

    // get bottom edge
    for (x = 0; x < w; x++) {
        for (y = h; y >= 0; y--) {
            if (buffer32[x + y * w] > 0) {
                if (y > y2)
                    y2 = y;
            }
        }
    }

    var rectObj = {
        x: x1,
        y: y1,
        w: x2 - x1,
        h: y2 - y1
    };


    ctx.clearRect(0, 0, w, h);
    // console.log(rectObj)
    if (onlyGetBounds) {
        return rectObj;
    }

    img.regX = rectObj.x + (rectObj.w / 2);
    img.regY = rectObj.y + (rectObj.h / 2);
    img.x = stage.canvas.width / 2; //rectObj.x + (rectObj.w / 2);
    img.y = rectObj.y + (rectObj.h / 2); //rectObj.y + (rectObj.y);
//return rectObj;
}

// smoke:

function getSmoke() {
    var smokeContainer = new createjs.Container();
    smokeContainer.smoke_1 = new createjs.Bitmap(loadedImages['smoke.png']);
    smokeContainer.smoke_1.setTransform(23.8, 70, 0.607, 0.607, 0, 0, 0, 0, 108);

    smokeContainer.smoke_2 = new createjs.Bitmap(loadedImages['smoke.png']);
    smokeContainer.smoke_2.setTransform(43.9, 70, 0.358, 0.358, 0, 0, 0, 0, 108);

    smokeContainer.smoke_3 = new createjs.Bitmap(loadedImages['smoke.png']);
    smokeContainer.smoke_3.setTransform(68.4, 70, 0.519, 0.519, 0, 0, 0, 68.5, 108);

    smokeContainer.addChild(smokeContainer.smoke_1, smokeContainer.smoke_2, smokeContainer.smoke_3);
    return smokeContainer;
}


function animateSmoke_left() {
    TweenLite.to(smoke_left.smoke_1, 4, {
        scaleX: 0.9,
        scaleY: 0.9,
        x: -25,
        ease: Linear.easeNone
    })
    TweenLite.to(smoke_left.smoke_1, 4, {
        scaleX: 1.5,
        scaleY: 1.5,
        x: -68,
        y: 95,
        alpha: 0,
        delay: 4,
        ease: Linear.easeNone
    })

    TweenLite.to(smoke_left.smoke_2, 4, {
        scaleX: 0.61,
        scaleY: 0.61,
        x: -10,
        ease: Linear.easeNone,
        delay: .2
    })
    TweenLite.to(smoke_left.smoke_2, 4, {
        scaleX: 1.4,
        scaleY: 1.4,
        x: -75,
        y: 80,
        alpha: 0,
        delay: 4.2,
        ease: Linear.easeNone
    })

    TweenLite.to(smoke_left.smoke_3, 4, {
        scaleX: 0.88,
        scaleY: 0.88,
        x: -15,
        ease: Linear.easeNone,
        delay: .4
    })
    TweenLite.to(smoke_left.smoke_3, 4, {
        scaleX: 1.34,
        scaleY: 1.34,
        x: -55,
        y: 82,
        alpha: 0,
        delay: 4.4,
        ease: Linear.easeNone,
        onComplete: resetSmoke,
        onCompleteParams: [smoke_left]
    })
}

function resetSmoke(smokeSide) {
    rollverStateReady = true;
    smokeSide.smoke_1.setTransform(23.8, 70, 0.607, 0.607, 0, 0, 0, 0, 108);
    smokeSide.smoke_2.setTransform(43.9, 70, 0.358, 0.358, 0, 0, 0, 0, 108);
    smokeSide.smoke_3.setTransform(68.4, 70, 0.519, 0.519, 0, 0, 0, 68.5, 108);

}

function animateSmoke_right() {
    TweenLite.to(smoke_right.smoke_1, 2, {
        scaleX: 0.9,
        scaleY: 0.9,
        x: 50,
        ease: Linear.easeNone
    })
    TweenLite.to(smoke_right.smoke_1, 2, {
        scaleX: 1.33,
        scaleY: 1.33,
        y: 95,
        alpha: 0,
        delay: 2,
        ease: Linear.easeNone
    })

    TweenLite.to(smoke_right.smoke_2, 2, {
        scaleX: 0.61,
        scaleY: 0.61,
        ease: Linear.easeNone,
        delay: .2
    })
    TweenLite.to(smoke_right.smoke_2, 2, {
        scaleX: 0.94,
        scaleY: 0.94,
        x: 70,
        y: 80,
        alpha: 0,
        delay: 2.2,
        ease: Linear.easeNone
    })

    TweenLite.to(smoke_right.smoke_3, 2, {
        scaleX: 0.88,
        scaleY: 0.88,
        x: 95,
        ease: Linear.easeNone,
        delay: .4
    })
    TweenLite.to(smoke_right.smoke_3, 2, {
        scaleX: 1.14,
        scaleY: 1.14,
        y: 82,
        alpha: 0,
        delay: 2.4,
        ease: Linear.easeNone
    })
}


function moveText(restingPos, callback, displacement) {
    var spacer = 1;
    displacement = typeof displacement !== 'undefined' ? displacement : 25;
    TweenLite.to(text1_a, .2, {
        css: {
            left: (restingPos - text1_a.clientWidth) / 2 + "px"
        },
        overwrite: 0
    });

    TweenLite.to(text1_b, .2, {
        css: {
            left: (restingPos - text1_b.clientWidth) / 2 + "px"
        }
    });

    if (frameCounter < copyObject.length - 1) {
        TweenLite.to(text1_a, 2, {
            css: {
                left: (restingPos - text1_a.clientWidth) / 2 + displacement + "px"
            },
            delay: .2,
            overwrite: 0,
            ease: Linear.easeNone
        })
        TweenLite.to(text1_a, .2, {
            css: {
                left: canvas.offsetWidth + 50 + "px"
            },
            delay: 2.2,
            overwrite: 0,
            ease: Cubic.easeOut
        })
        TweenLite.to(text1_b, 2, {
            css: {
                left: ((restingPos - text1_b.clientWidth) / 2 - displacement) + "px"
            },
            delay: .2,
            overwrite: 0,
            ease: Linear.easeNone
        })
        TweenLite.to(text1_b, .2, {
            css: {
                left: -(canvas.offsetWidth + 50) + "px"
            },
            delay: 2.2,
            overwrite: 0,
            ease: Cubic.easeOut,
            onComplete: function() {
                if (typeof callback == "function") {
                    callback();
                }
            }
        })
    } else {
        if (typeof callback == "function") {
            callback();
        }
    }


}
