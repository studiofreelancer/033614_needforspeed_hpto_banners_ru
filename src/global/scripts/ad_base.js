var ctx,
    canvas,
    stage,
    middleContainer,
    bottomContainer,
    topContainer;

var fps = 24;
var loader;
var loadedImages = {};
var manifest = [];

var imagesContent = {}; // overwritten in main.js
var loopCount = 0;
var numOfLoops = 3; // default - can be overwritten in main.js
var platformType = "";

var libraryPool = ["https://code.createjs.com/easeljs-0.8.2.min.js",
    "https://code.createjs.com/preloadjs-0.6.2.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenLite.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/plugins/CSSPlugin.min.js"]; // external Libraries - mandatory for this template. 

var pathCheck = location.hostname === "localhost" ? "assets/" : "assets/";

//TODO: customise this to load Enabler in case of DC or clickTag in case of others

function init(pType) {
    platformType = pType;
    console.log(pType)
    if (platformType === "DCS") { // DOUBLECLICK

        if (!Enabler.isInitialized()) {
            Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitialized);
        } else {
            enablerInitialized();
        }
        function enablerInitialized() {
            if (Enabler.isPageLoaded()) {
                pageLoadedHandler();
            } else {
                Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler);
            }
        }
    } else if (platformType === "SIZMEK") { // SIZMEK
        console.log('ED READY', EB)
        if (!EB.isInitialized()) {
            EB.addEventListener(EBG.EventName.EB_INITIALIZED, pageLoadedHandler);

        } else {

            pageLoadedHandler();
        }
    } else {
        //gets clickTAG variable, if it is not defined (e.g. banner is being tested locally) it will fallback to example.com
        pageLoadedHandler(); // GENERIC
    }
}

function getClickTag() { // generic clicktag in case is not a DCS creative:
    return window.clickTag || 'http://www.example.com';
}

// load external dependencies/libraries: CREATEJS, GSAP
function pageLoadedHandler() {
    head.load(
        libraryPool, allExternalScriptsLoaded);
}

// **** load external assets:
function allExternalScriptsLoaded() {
    canvas = document.getElementById("canvas");
    stage = new createjs.Stage(canvas);
    ctx = canvas.getContext("2d");

    //for retina screens:

//     if (window.devicePixelRatio) {
//     stage.scaleX = stage.scaleY = devicePixelRatio;
//     canvas.width = canvas.offsetWidth * devicePixelRatio;
//     canvas.height = canvas.offsetHeight * devicePixelRatio;
// }

    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.setFPS(fps);
    createjs.Ticker.addEventListener("tick", stage);
    // Enabler.counter("BANNER_VARS_SET_UP");
    setupManifest();
    prepAssets();
}

function prepAssets() {
    // console.log('manifest ', manifest.length)
    // CTA click listener:
    // add all elements from the assets libs:
    if (manifest.length > 0) {
        // for (var key in imagesContent) {
        //     var obj = {};
        //     obj.id = key;
        //     obj.src = imagesContent[key]['Url'];
        //     manifest.push(obj);
        // }

        // console.log(' window.location.protocol', window.location.protocol);
        loader = new createjs.LoadQueue(window.location.protocol === "file:" ? false : true, null, true);
        loader.addEventListener("fileload", handleFileLoad);
        loader.addEventListener("complete", handleComplete);
        loader.loadManifest(manifest);
        document.getElementById('exitButton').addEventListener('click', onCtaClick);
    } else {
        document.getElementById('preloader').innerHTML = "ERROR:<br />var <b><i>imageContent</i></b> is empty or corrupted. Please add images and try again!!"
        document.getElementById('preloader').style.backgroundImage = "none";
        document.getElementById('preloader').style.width = "inherit";
        document.getElementById('preloader').style.left = 0;
        document.getElementById('preloader').style.marginLeft = 0;
        document.getElementById('preloader').style.textAlign = "center";
        document.getElementById('preloader').style.color = "red";
        document.getElementById('preloader').style.padding = "20px";
        document.getElementById('preloader').style.boxSizing = "border-box";
    }
}

function handleFileLoad(evt) {
    if (evt.item.type == "image") {
        loadedImages[evt.item.id] = evt.result;
        loadedImages[evt.item.id].crossOrigin = 'anonymous';
    // console.log(loadedImages[evt.item.id]);
    }
}

function handleComplete(evt) {
    // Enabler.counter("ASSETS_LOADED");
    // hide the preloader
    document.getElementById('preloader').style.visibility = 'hidden';

    //adding elements to the canvas
    topContainer = new createjs.Container();
    bottomContainer = new createjs.Container();
    middleContainer = new createjs.Container();

    stage.addChild(bottomContainer);
    stage.addChild(middleContainer);
    stage.addChild(topContainer);

    addCustomAssets(); // function that adds 
}

// CLICK TAG EXIT:
function onCtaClick() {
    if (platformType === "DCS") { // DOUBLECLICK
        Enabler.exit("clickThrough");
    } else if (platformType === "SIZMEK") { // SIZMEK
        EB.clickthrough("clickThrough");
    } else if (platformType === "ADFORM") { // ADFORM
        var clickTAGvalue = dhtml.getVar('clickTAG', 'http://www.example.com');
        var landingpagetarget = dhtml.getVar('landingPageTarget', '_blank');
        window.open(clickTAGvalue, landingpagetarget);
        return;
    } else {
        window.open(getClickTag(), '_blank'); // GENERIC and DCM
    }
}