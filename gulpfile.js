var gulp = require('gulp');
var pkg = require('./package.json');
var fs = require('fs');
var path = require('path');
var merge = require('merge-stream');
var filesize = require('gulp-size');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var wait = require('gulp-wait');
var replace = require('gulp-replace');
var argv = require('yargs').argv;
var eslint = require('gulp-eslint');
var del = require('del');
var buffer = require('vinyl-buffer');
var chalk = require('chalk');

var uglify = require('gulp-uglify');
var runSequence = require('run-sequence');
var rename = require("gulp-rename");
// Console Colors
var cErr = chalk.red;
var cInfo = chalk.dim.gray;
var cTask = chalk.bold.green;

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


// **** FTP Automation *************************
var gutil = require('gulp-util');
var ftp = require('vinyl-ftp');

/** Configuration **/
var user = process.env.USER; //"dev@freedmaninternational.com";  
var password = process.env.PWD; //"4grJMdvkrG6V";  
var host = '94.228.32.169';
var port = 21;
var localFilesGlob = ['./dev/**/**'];
var remoteFolder = '/preview/' + process.env.REMOTE;

var screenshot = require('screenshot-stream');

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
    return ftp.create({
        host: host,
        port: port,
        user: user,
        password: password,
        parallel: 5,
        log: gutil.log
    });
}

/**
 * Deploy task.
 * Copies the new files to the server
 *
 * Usage: `USER=someuser PWD=somepwd REMOTE=remoteFolder gulp ftp-deploy`
 */
gulp.task('ftp-deploy', function() {

    var conn = getFtpConnection();
    return gulp.src(localFilesGlob, {
        base: '.',
        buffer: false
    })
        .pipe(conn.newer(remoteFolder)) // only upload newer files 
        .pipe(conn.dest(remoteFolder))
        .on('end', function() {
            // gutil.log('https://www.freedmaninternational.com/automationtest/_pr.php?PATH='+process.env.REMOTE);
            console.log(cErr('https://www.freedmaninternational.com/preview/_pr.php?PATH=' + process.env.REMOTE));
        });
});

// *************************************************

function getFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function(file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}

function makesprite(variant, size, retina) {
    var folder = variant + '/' + size + '/';
    var origin = 'src/variants/' + folder;
    var dest = origin;
    var spriteData;

    if (!retina) {
        spriteData = gulp.src(origin + 'assets/sprites/*.*') // source path of the sprite images
            .pipe(spritesmith({
                imgName: 'txtsprite.png',
                imgPath: 'assets/txtsprite.png',
                cssName: 'txtsprite.css',
                padding: 1
            }))
            .on('error', function(error) {
                console.log(cErr(error));
            });
    } else {
        spriteData = gulp.src(origin + 'assets/sprites/*.*') // source path of the sprite images
            .pipe(spritesmith({
                retinaSrcFilter: [origin + 'assets/sprites/*@2x.*'],
                imgName: 'txtsprite.png',
                retinaImgName: 'txtsprite@2x.png',
                imgPath: 'assets/txtsprite.png',
                retinaImgPath: 'assets/txtsprite@2x.png',
                cssName: 'txtsprite.css',
                padding: 1
            }))
            .on('error', function(error) {
                console.log(cErr(error));
            });
    }

    spriteData.css.pipe(gulp.dest(dest)); // output path for the CSS

    spriteData.img
        .pipe(buffer())
        .pipe(imagemin({ // compress PNG
            progressive: true,
            use: [pngquant({
                quality: '40-65',
                speed: 4
            })] // quality settings
        }))
        .pipe(gulp.dest(dest + 'assets/')); // output path for the sprite
}

gulp.task('default', function() {
    console.info(cInfo('version: '), pkg.version);
});

// Cleans the dist and dev folders
gulp.task('clean', function() {
    del(['dist/**/*', 'dev/**/*'], {
        force: true
    })
        .then(function(paths) {
            console.log(cTask('Cleaning... >>>>>\n'), cInfo(paths.join('\n ')));
        });
});

// lints the js files for errors
gulp.task('lint', function() {
    console.log(cTask('Linting files...'));
    // ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.
    return gulp.src(['src/**/*.js'])
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});


/** take the src folder, iterate over the structure to two depths assuming: first level = variants, second level = sizes.
  you can pass arguments on the command line -v to specify a single variant and -s to specify a single size. These
  arguments can be used together or seperately
  @param -v [variant folder name] (optional)
  @param -s [size folder name] (optional)
  @param -r [retina images] (optional, default true) requires retina images exist for all images in the spritesheet `[file]@2x.[ext]`
  @usage gulp makesprites -v myvariant -s mysize -r true
**/

gulp.task('makesprites', function() {
    console.log(cTask('Making sprite sheets...'));
    console.log(cInfo('variant'), argv.v, cInfo(', size'), argv.s);
    var retina = (argv.r === undefined) ? true : ((argv.r === 'false') ? false : true);
    var variants = (argv.v === undefined) ? getFolders('src/variants/') : [argv.v];

    for (var i = 0, vl = variants.length; i < vl; i++) {
        var variant = variants[i];
        var sizes = (argv.s === undefined) ? getFolders('src/variants/' + variant) : [argv.s];

        for (var j = 0, sl = sizes.length; j < sl; j++) {
            var size = sizes[j];
            makesprite(variant, size, retina);
        }
    }
});

// gulp.task('screenshot', function() {
//     var variants = getFolders('dev/');

//     for (var i = 0; i < variants.length; i++) {

//         var size = getFolders('dev/' + variants[i]);

//         for (var j = 0; j < size.length; j++) {
//             console.log(size[j])
//             var sizename = size[j].split("_")[0];

//             var stream = screenshot('./dev/' + variants[i] + "/" + sizename + '_DCM/index.html', sizename, {
//                 crop: true,
//                 delay: 25,
//                 format: "jpg"
//             });
//             stream.pipe(fs.createWriteStream("dev/" + variants[i] + "/" + sizename + '_backup.jpg'));
//         }
//     }
// });


var shotArray = [];
var marketCount = 0;
var sizeCount = 0;
gulp.task('screenshot', function() {
    var variants = getFolders('dev/');
    for (var i = 0; i < variants.length; i++) {
        var obj = {};
        obj.market = variants[i];
        obj.sizes = [];
        var size = getFolders('dev/' + variants[i]);

        for (var j = 0; j < size.length; j++) {
            obj.sizes.push(size[j]);
        }
        shotArray.push(obj);
    }
    capture()
});
// capture the image:
function capture() {
    console.log(chalk.bold.yellow(shotArray[marketCount].market + " generating backup for " + shotArray[marketCount].sizes[sizeCount] + " .... be patient now!"));
    var sizename = shotArray[marketCount].sizes[sizeCount].split("_")[0];
    var stream = screenshot('./dev/' + shotArray[marketCount].market + "/" + sizename + '_DCM/index.html', sizename, {
        crop: true,
        delay: 10,
        format: "jpg"
    });
    // shotArray.push(str)
    var fsStream = fs.createWriteStream("dev/" + shotArray[marketCount].market + "/" + pkg.meta.campaign + "_" + sizename + "_" + shotArray[marketCount].market + "_backup.jpg");
    stream.pipe(fsStream);
    fsStream.on('close', function() {
        if (sizeCount < shotArray[marketCount].sizes.length - 1) {
            sizeCount++;
        } else {
            console.log(chalk.bold.red(' ------------- all backups created for ' + shotArray[marketCount].market + " -------------\n"));
            sizeCount = 0;
            marketCount++;
        }
        if (marketCount < shotArray.length) capture();
        console.log(chalk.cyan('okay!! backup generated.... moving on the next one in the chain. Will keep you posted!'));
    });
}


// take the src folder, iterate over the structure to two depths assuming: first level = variants, second level = sizes.
// builds the src files into the dev folder. Concats JS and css
// gulp.task('build', ['clean','lint'], function () {
gulp.task('build', function(callback) {
    var fullmerge = merge();
    runSequence('clean', 'lint', function() {
        console.log(cTask('Building Banners... '));
        // console.log(cTask("META >>>>>>>>>>>>>>>>>>>> ",getMetaInfo()));

        var variants = getFolders('src/variants/');

        // var interval = setTimeout(function(){
        for (var i = 0, vl = variants.length; i < vl; i++) {
            var variant = variants[i];
            var sizes = getFolders('src/variants/' + variant);

            for (var j = 0, sl = sizes.length; j < sl; j++) {
                var size = sizes[j];
                var folder = variant + '/' + size + '/';
                var origin = 'src/variants/' + folder;
                var backupImg = 'src/variants/' + variant + "/";
                var dest = 'dev/' + folder;

                var merged = merge();

                // move images
                gulp.src(['src/global/assets/**', origin + 'assets/**', '!' + origin + 'assets/sprites/', '!' + origin + 'assets/sprites/**'])
                    .pipe(gulp.dest(dest + 'assets/'));

                // preloader:
                gulp.src([origin + "*.gif", origin + "*.jpg", origin + "*.png", 'src/global/default/*.gif', "src/global/default/*.otf", "src/global/default/*.eot"])
                    .pipe(gulp.dest(dest));

                //backup:
                gulp.src([backupImg + size.substring(0, size.indexOf('_') + 1) + "*.jpg"])
                    .pipe(gulp.dest('dev/' + variant + "/"));


                // concat the styles
                var styleStream = gulp.src(['src/global/styles/*.css', origin + '*.css'])
                    .pipe(concat('screen.css'))
                    .pipe(gulp.dest(dest));
                merged.add(styleStream);


                // concat the javascript
                // var scriptStream = gulp.src(['src/global/scripts/**/*.js', origin + '*.js'])
                var scriptStream = gulp.src(['src/global/scripts/ad_base.js', 'src/global/scripts/head.load.min.js', 'src/global/scripts/utilities.js', 'src/global/scripts/' + "main_" + size.substring(0, size.indexOf('_')) + '.js', origin + '*.js'])
                    .pipe(concat('scripts.min.js'))
                    .pipe(uglify())
                    .pipe(gulp.dest(dest));
                merged.add(scriptStream);


                /**
                - ww/hh - sets the width and height of the banner based on the naming convention 
                  - takes the dimensions from folder name, 
                  - i.e., 300px for width and 250px for height from 300x250 respectively.

                - category - is the appended name of the platforms i.e. 300x250_DCS for DoubleClick Studio, 300x250_DCM etc. Default value is "generic"
                  - the naming automatically adds the clickTag proceedure accordingly to the creatives.  
                
                - platformCode - code injected dynamically specific for each kind of platforms - DoubleClick, Sizmek, Generic etc.

                **/

                var ww = size.substring(0, size.indexOf('x'));
                var hh = size.substring(size.indexOf('x') + 1, size.indexOf('_') !== -1 ? size.indexOf('_') : size.length);
                var category = size.indexOf('_') !== -1 ? size.substring(size.indexOf('_') + 1, size.length) : "generic";
                var platformCode = "";
                var window = {}; // hack (workable) to add script in case of ADFORM platform

                // add a switch condition here to add appropriate scripts here:
                switch (category) {
                case "DCS":
                    platformCode = "<script src='https://s0.2mdn.net/ads/studio/Enabler.js'></script>";
                    break;
                case "generic":
                    platformCode = "<meta name='ad.size' content='width=" + ww + ",height=" + hh + "'><script type='text/javascript'>var clickTag='http://www.google.com'</script>";
                    break;
                case "DCM":
                    platformCode = "<meta name='ad.size' content='width=" + ww + ",height=" + hh + "'><script type='text/javascript'>var clickTag='http://www.google.com'</script>";
                    break;
                case "SIZMEK":
                    platformCode = "<script type='text/javascript' src='https://secure-ds.serving-sys.com/BurstingScript/EBLoader.js'></script>";
                    break;
                case "ADFORM":
                    platformCode = '<script src="' + (window.API_URL || 'https://s1.adform.net/banners/scripts/rmb/Adform.DHTML.js?bv=' + Math.random()) + '"><\/script>';
                    var manifestStream = gulp.src('src/global/default/manifest.json')
                        .pipe(replace('{{title}}', size))
                        .pipe(replace('{{width}}', ww))
                        .pipe(replace('{{height}}', hh))
                        .pipe(gulp.dest(dest));
                    break;
                }

                console.log(cErr("++++ " + category));

                // inject the style and JS as well as meta info
                var injectStream = gulp.src('src/global/default/*.html')
                    .pipe(inject(merged, {
                        ignorePath: dest,
                        addRootSlash: false
                    }))
                    .pipe(replace('<!-- inject:meta -->', platformCode))
                    .pipe(replace('{{author}}', pkg.author))
                    .pipe(replace('{{platformType}}', category))
                    .pipe(replace('{{description}}', pkg.description))
                    .pipe(replace('{{version}}', pkg.version))
                    .pipe(replace('{{title}}', pkg.meta.client + ' ' + pkg.meta.campaign + ' | ' + variant + ' | ' + size + ' | ' + pkg.version))
                    .pipe(replace('{{width}}', ww))
                    .pipe(replace('{{height}}', hh))
                    .pipe(gulp.dest(dest))
                    .pipe(wait(100)); // insert a slight pause so the file system can write successfully before we zip if this task is part of the zip chain

                fullmerge.add(injectStream);
            }
        }
        // },400); // due to large number of files to delete and rebuild second delay is added

    });
    return fullmerge;
});

// take the src folder, iterate over the structure to two depths assuming: first level = variants, second level = sizes.
// create zips per size for each variant and place in the dist folder
gulp.task('zip', ['build'], function() {
    console.log(cTask('Zipping Banners'));
    var zip = require('gulp-zip'); // zip files
    // var date = new Date().toISOString().replace(/[^0-9]/g, '');
    var merged = merge();
    var variants = getFolders('dev/');

    for (var i = 0, vl = variants.length; i < vl; i++) {
        var variant = variants[i];
        var sizes = getFolders('dev/' + variant);

        for (var j = 0, sl = sizes.length; j < sl; j++) {
            var size = sizes[j];
            var folder = 'dev/' + variant + '/' + size + '/';
            var filename = pkg.meta.client + '_' + pkg.meta.campaign + '_' + variant + '_' + size + '_v' + pkg.version + '.zip';

            // keep directory structure
            var zipStream = gulp.src(folder + '**/*')
                .pipe(zip(filename))
                .pipe(filesize({
                    title: filename,
                    showFiles: true
                }))
                .pipe(gulp.dest('dist'));

            merged.add(zipStream);
        }
    }
    return merged;
});

gulp.task('rename', function() {
    var variants = getFolders('dev/');
    var merged = merge();
    for (var i = 0, vl = variants.length; i < vl; i++) {
        var variant = variants[i];
        var sizes = getFolders('dev/' + variant);

        for (var j = 0, sl = sizes.length; j < sl; j++) {
            var size = sizes[j];
            var folder = 'dev/' + variant + '/' + size + '/';
            var backups = 'dev/' + variant + "/" + size.split("_")[0] + '_backup.jpg';
            console.log(folder);


            fs.rename(folder, "dev/" + variant + "/" + pkg.meta.campaign + "_" + size.split("_")[0] + "_" + variant, function(err) {
                if (err) {
                    throw err;
                }

            });
        }
    }
})

/** Development-optimized workflow with browsersync
  Clean/build first, then serve and watch
  @usage gulp serve
**/
gulp.task('serve', ['build'], function() {
    console.log(cTask('Browser Sync ...'));

    browserSync.init({
        server: './dev',
        directory: true
    });

    // If changes are made to global or variant html, js or css, or background
    // images, rebuild everything and reloads
    // the browser
    gulp.watch(
        ['src/global/styles/*.css',
            'src/global/scripts/**/*.js',
            'src/global/default/**/*.html',
            'src/variants/*',
            'src/variants/**/*.html',
            // 'src/variants/**/*.js',
            'src/variants/**/*.css'],
        ['build', reload])
        .on('error', function(err) {
            // console.log('error caught????????????', err)
        });

    gulp.watch(['src/variants/**/assets/*.jpg',
        'src/variants/**/assets/*.png',
        'src/variants/**/assets/*.gif'], function(file) {
        var market = file.path.split("variants/")[1].split("/")[0];
        var size = file.path.split("variants/")[1].split("/")[1].split("/")[0];
        console.log(market, size)

        gulp.src(file.path)
            .pipe(gulp.dest("dev/" + market + "/" + size + "/assets"));
        reload();
    })
    // .on(['add', 'changed', 'unlink'], function(err) {
    //     console.log('error caught????????????', err)
    // });

    gulp.watch(['src/variants/**/*.js'], function(file) {
        var market = file.path.split("variants/")[1].split("/")[0];
        var size = file.path.split("variants/")[1].split("/")[1].split("/")[0];

        gulp.src(['src/global/scripts/ad_base.js', 'src/global/scripts/head.load.min.js', 'src/global/scripts/utilities.js', 'src/global/scripts/' + "main_" + size.substring(0, size.indexOf('_')) + '.js', file.path])
            .pipe(concat('scripts.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest("dev/" + market + "/" + size));
        reload();
    });
});
